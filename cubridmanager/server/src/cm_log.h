/*
 * Copyright (C) 2008 Search Solution Corporation. All rights reserved by Search Solution.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#ifndef __CM_LOG__H__
#define __CM_LOG__H__
#include <stdio.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>

#if defined(WINDOWS)
#include <process.h>
#include <io.h>
#else
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#endif

#include "cm_config.h"

using namespace std;

#define DEFAULT_LOG_LEVEL xWARN

#ifdef WINDOWS
#define mutex_t                           CRITICAL_SECTION
#define mutex_init(mutex)            InitializeCriticalSection(&mutex)
#define mutex_lock(mutex)          EnterCriticalSection(&mutex)
#define mutex_unlock(mutex)      LeaveCriticalSection(&mutex)
#define mutex_destory(mutex)    DeleteCriticalSection(&mutex)
#else
#define mutex_t                           pthread_mutex_t
#define mutex_init(mutex)            pthread_mutex_init(&mutex, NULL)
#define mutex_lock(mutex)          pthread_mutex_lock(&mutex)
#define mutex_unlock(mutex)       pthread_mutex_unlock(&mutex)
#define mutex_destory(mutex)     pthread_mutex_destroy(&mutex)
#endif

#define MAX_LOG_FILE     (4 * 1024 * 1024)

class CLog
{
public:
  enum LOGLEVEL
  {
    xFATAL = 0,
    xERROR = 1,
    xWARN = 2,
    xINFO = 3,
    xDEBUG = 4
  };

protected:
    CLog ()
  {
    char file[256];
      conf_get_dbmt_file (FID_CMS_ACCESS_LOG, file);
      sLogLevel = CLog::xWARN;
      m_lTruncate = MAX_LOG_FILE;
      m_pLogFile = fopen (file, "a");
      mutex_init (m_cs);
  };

  CLog (const char *plogfile, bool bappend = TRUE, LOGLEVEL loglevel =
	CLog::DEFAULT_LOG_LEVEL, long maxloglen = MAX_LOG_FILE)
  {
    sLogLevel = loglevel;
    m_lTruncate = maxloglen;
    m_pLogFile = fopen (plogfile, bappend ? "a" : "w");
    mutex_init (m_cs);
  }
public:
  ~CLog ()
  {
    if (m_pLogFile)
      {
	fclose (m_pLogFile);
	m_pLogFile = NULL;
      }

    mutex_destory (m_cs);
  }

private:
  LOGLEVEL _logLevel ()
  {
    return sLogLevel;
  }

  string _get_format_time ()
  {
    char buff[128] = "unkonwn";
    time_t lt;
    time (&lt);
    tm *t = localtime (&lt);
    if (t)
      strftime (buff, 128, "%Y%m%d %H:%M:%S", t);
    return string (buff);
  }

public:
  static CLog *GetInstance ()
  {
    char log_path[PATH_MAX];
    static CLog *instance = NULL;
    if (NULL == instance)
      {
	conf_get_dbmt_file (FID_CMS_ACCESS_LOG, log_path);
	instance = new CLog (log_path);
      }
    return instance;
  }

  void setLogLevel (const unsigned int level)
  {
    sLogLevel = (LOGLEVEL) level;
  }

  void formatLog (const int iPriority, const char *fmt, ...)
  {
    if (!m_pLogFile)
      return;

    //check log level
    if (iPriority > _logLevel ())
      return;

    //Trancate if the file grow too large
    if (ftell (m_pLogFile) > m_lTruncate)
      rewind (m_pLogFile);

    //format log message
    const char *strLevel;
    switch (iPriority)
      {
      case CLog::xFATAL:
	strLevel = "FATAL";
	break;
      case CLog::xERROR:
	strLevel = "ERROR";
	break;
      case CLog::xWARN:
	strLevel = " WARN";
	break;
      case CLog::xINFO:
	strLevel = " INFO";
	break;
      case CLog::xDEBUG:
      default:
	strLevel = "DEBUG";
	break;
      }

    //format log data
    size_t size = 1024;
    char *buffer = new char[size];
    memset (buffer, 0, size);

    while (1)
      {
	va_list args;
	va_start (args, fmt);
#ifdef _WIN32
	int n = _vsnprintf (buffer, size, fmt, args);
#else
	int n = vsnprintf (buffer, size, fmt, args);
#endif
	va_end (args);
	if ((n > -1) && (static_cast < size_t > (n) < size))
	  {
	    break;
	  }

	size = (n > -1) ? n + 1 : size * 2;
	delete[]buffer;
	buffer = new char[size];
	memset (buffer, 0, size);
      }

    mutex_lock (m_cs);
    ::fprintf (m_pLogFile, "[%s] [%s] [%6d] %s\n",
	       _get_format_time ().c_str (), strLevel, getpid (), buffer);
    fflush (m_pLogFile);
    mutex_unlock (m_cs);
    delete[]buffer;
  }

private:
  LOGLEVEL sLogLevel;
  FILE *m_pLogFile;
  long m_lTruncate;
  mutex_t m_cs;
};

#define STRINGIZE2(s) #s
#define STRINGIZE(s)  STRINGIZE2(s)

#ifndef LogPrefix
#define LogPrefix(fmt) std::string("[").append(__FUNCTION__).append(":").append(STRINGIZE(__LINE__)).append("] ").append(fmt).c_str()
#endif

#define LOG_DEBUG(fmt, ...) \
    CLog::GetInstance()->formatLog(CLog::xDEBUG, LogPrefix(fmt), ##__VA_ARGS__)

#define LOG_INFO(fmt, ...) \
    CLog::GetInstance()->formatLog(CLog::xINFO, LogPrefix(fmt),  ##__VA_ARGS__)

#define LOG_WARN(fmt, ...) \
    CLog::GetInstance()->formatLog(CLog::xWARN, LogPrefix(fmt), ##__VA_ARGS__)

#define LOG_ERROR(fmt, ...) \
    CLog::GetInstance()->formatLog(CLog::xERROR, LogPrefix(fmt), ##__VA_ARGS__)

#define LOG_FATAL(fmt, ...) \
    CLog::GetInstance()->formatLog(CLog::xFATAL, LogPrefix(fmt), ##__VA_ARGS__)

#endif
