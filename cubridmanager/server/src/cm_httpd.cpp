/*
 * Copyright (C) 2008 Search Solution Corporation. All rights reserved by Search Solution.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

/*
*  cm_httpd.cpp
*/

#include <sys/types.h>
#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/timeb.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <assert.h>
#include <signal.h>
#include <string.h>
#include <event2/event.h>
#include <evhttp.h>
#include <event2/buffer.h>
#include <event2/http_struct.h>

#ifdef WINDOWS
#include <windows.h>
#include "cm_win_wsa.h"
#else
#include <arpa/inet.h>
#endif
#include "cm_cci_interface.h"
#include "cm_server_interface.h"
#include "cm_server_extend_interface.h"
#include "cm_log.h"
#include "cm_mon_stat.h"

//#include "cm_utf8.h"
using namespace std;

T_EMGR_VERSION CLIENT_VERSION = EMGR_MAKE_VER (8, 4);

#define DEFAULT_THRD_NUM	        64
#define DEFAULT_HTTP_TIMEOUT		600	//second
#define NUM_OF_FILES_IN_URL         3

struct worker_context
{
  struct event_base *base;
  struct evhttp *httpd;
  struct event *timer;
  bool first;
#ifdef WINDOWS
  HANDLE ths;
#else
  pthread_t ths;
#endif
};

int
bind_socket (int port)
{
  int r;
  int nfd;
  struct sockaddr_in addr;
  int one = 1;
  int flags = 1;
  nfd = socket (AF_INET, SOCK_STREAM, 0);
  if (nfd < 0)
    return -1;

  setsockopt (nfd, SOL_SOCKET, SO_REUSEADDR, (char *) &one, sizeof (int));

  memset (&addr, 0, sizeof (addr));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  addr.sin_port = htons (port);
  r = bind (nfd, (struct sockaddr *) &addr, sizeof (addr));
  if (r < 0)
    {
      close (nfd);
      return -1;
    }
  r = listen (nfd, 10240);
  if (r < 0)
    {
      close (nfd);
      return -1;
    }
#ifdef WINDOWS
  ioctlsocket (nfd, FIONBIO, (unsigned long *) &flags);
#else
  if ((flags = fcntl (nfd, F_GETFL, 0)) != -1)
    fcntl (nfd, F_SETFL, flags | O_NONBLOCK);
#endif

  return nfd;
}

#ifdef WINDOWS
DWORD WINAPI
dispatch_thread (void *arg)
#else
void *
dispatch_thread (void *arg)
#endif
{
  struct worker_context *work_ctx = (struct worker_context *) arg;
  event_base_dispatch ((struct event_base *) work_ctx->base);

  if (work_ctx->timer)
  {
      event_free (work_ctx->timer);
  }
  if (work_ctx->httpd)
  {
    evhttp_free (work_ctx->httpd);
  }
  if (work_ctx->base)
  {
    event_base_free (work_ctx->base);
  }
#ifdef WINDOWS
  return 0;
#else
  return NULL;
#endif
}

void
cub_add_private_param (struct evhttp_request *req, Json::Value & root)
{
  root["_CLIENTIP"] = req->remote_host;
  root["_CLIENTPORT"] = req->remote_port;
  root["_STAMP"] = 1;
  root["_SVRTYPE"] = "fsvr";
  root["_PROGNAME"] = "cub_cmserver";
}

void
cub_generic_request_handler (struct evhttp_request *req, void *arg)
{
  struct evbuffer *input;
  struct evkeyvalq *headers;
  char *body;
//    char *inustr, *outustr;
  string task_name;
  int len;
  struct evbuffer *evb;
  Json::Value root, response;
  Json::Reader reader;
  Json::StyledWriter writer;

  input = evhttp_request_get_input_buffer (req);
  if (input == NULL)
    {
      evhttp_send_reply (req, HTTP_BADREQUEST, "", NULL);
      return;
    }

  len = evbuffer_get_length (input);
  body = (char *) malloc (len + 1);
  if (body == NULL)
    {
      evhttp_send_reply (req, HTTP_BADREQUEST, "", NULL);
      return;
    }


  evbuffer_copyout (input, body, len);
  body[len] = 0;
  //inustr = utf8_decode (body);
  LOG_INFO ("%s", body);
  if (!reader.parse (body, root))
    {
      free (body);
      //utf8_clean(inustr);
      evhttp_send_reply (req, HTTP_BADREQUEST, "Error JSON format", NULL);
      return;
    }

  cub_add_private_param (req, root);

  if (!strcmp ((char *) arg, "cci"))
    cub_cci_request_handler (root, response);
  else if (!strcmp ((char *) arg, "cm_api"))
    cub_cm_request_handler (root, response);

  //outustr = utf8_encode(writer.write(response).c_str());
  //printf("---------------------\n%s\n", outustr);
  evb = evbuffer_new ();
  if (NULL == evb)
    {
      free (body);
      //utf8_clean(inustr);
      //utf8_clean(outustr);
      return evhttp_send_reply (req, HTTP_BADREQUEST, "", NULL);
    }

  headers = evhttp_request_get_output_headers (req);
  if (headers)
    evhttp_add_header (headers, "Content-Type", "text/plain;charset=utf-8");

  evbuffer_add_printf (evb, "%s", writer.write (response).c_str ());
  evhttp_send_reply (req, HTTP_OK, "OK", evb);
  evbuffer_free (evb);
  free (body);
  //utf8_clean(inustr);
  //utf8_clean(outustr);

  return;
}

static int cub_loop_flag = 1;

void
cub_ctrl_request_handler (struct evhttp_request *req, void *arg)
{
  evhttp_send_reply (req, HTTP_OK, "", NULL);
  cub_loop_flag = 0;
  return;
}

void
cub_post_request_handler (struct evhttp_request *req, void *arg)
{
	string post_msg = "{ \"success\" : true }";
    string req_uri(req->uri);
    char token[TOKEN_ENC_LENGTH];
    size_t token_pos;
    int code;
    string reason;
    size_t fname_pos = 0;
    size_t tmp_pos = 0;
    struct evbuffer *evb = evbuffer_new();

    code = HTTP_OK;
    reason = "OK";
    token[0] = '\0';
    token_pos = 0;

	string cookie(evhttp_find_header(req->input_headers, "COOKIE"));
    token_pos = cookie.find("token=");
    if (token_pos == string::npos)
    {
        goto send_nok_reply;
    }

    cookie.copy(token, TOKEN_ENC_LENGTH - 1, token_pos + strlen("token="));
    token[TOKEN_ENC_LENGTH - 1] = '\0';
    if (ext_ut_validate_token(token))
    {
        goto send_reply;
    }

    for (int index = 0; index < NUM_OF_FILES_IN_URL; ++index)
    {
        char fname[PATH_MAX];
        string fname_path = string(sco.dbmt_tmp_dir) + "/";
        fname[0] = '\0';
        fname_pos = req_uri.find("fname=", tmp_pos);
        if (fname_pos == string::npos && index == 0)
        {
            goto send_nok_reply;
        }
        else if (fname_pos == string::npos)
        {
            goto send_reply;
        }

        fname_pos += strlen("fname=");
        tmp_pos = req_uri.find("&", fname_pos);
        if (tmp_pos == string::npos)
        {
            tmp_pos = req_uri.length();
        }
        req_uri.copy(fname, tmp_pos - fname_pos + 1, fname_pos);
        fname[tmp_pos - fname_pos] = '\0';
        if (strcmp(fname, "") != 0 || strcmp(fname, "&") != 0)
        {
            if (strstr(fname, "..") || strstr(fname, "\\") || strstr(fname, "/"))
            {
                continue;
            }
            fname_path += fname;
            unlink(fname_path.c_str());
        }
    }

send_nok_reply:
    post_msg = "{ \"failure\" : true }";
    code = HTTP_NOTFOUND;
    reason = "NOK";

send_reply:
    if (evb)
    {
        evbuffer_add_printf(evb, "%s", post_msg.c_str());
    }
    evhttp_send_reply (req, code, reason.c_str(), evb);
    if (evb)
    {
        evbuffer_free (evb);
    }
    return;
}

void
cub_timeout_cb (evutil_socket_t fd, short event, void *arg)
{
  struct worker_context *work_ctx = (struct worker_context *) arg;
  if (!cub_loop_flag)
    event_base_loopexit (work_ctx->base, NULL);
}


/**
 * @brief callback function to gather monitoring data
 */
void
cub_monitor_stat_cb (evutil_socket_t fd, short event, void *arg) {
    struct timeval stat_tv = { 1, 0 };

    struct worker_context *work_ctx = (struct worker_context *) arg;
    if (!cub_loop_flag)
    {
        event_base_loopexit (work_ctx->base, NULL);
        return;
    }

    // [CUBRIDSUS-11917]sleep the thread for a while when the CUBRID is starting
    if (work_ctx->first)
    {
        SLEEP_SEC(MIN_INTERVAL - 1);
        work_ctx->first = false;
    }

    if (sco.iSupportMonStat == TRUE) 
    {
        (cm_mon_stat::get_instance())->gather_mon_data();
    }

    evtimer_add(work_ctx->timer, &stat_tv);
}

void
cub_auto_jobs_cb (evutil_socket_t fd, short event, void *arg)
{
  struct timeval tv = { sco.iMonitorInterval, 0 };
  Json::Value request;
  Json::Value response;
  struct worker_context *work_ctx = (struct worker_context *) arg;
  if (!cub_loop_flag)
  {
    event_base_loopexit (work_ctx->base, NULL);
    return;
  }
  ext_exec_auto_jobs(request,  response);
  evtimer_add(work_ctx->timer, &tv);
}

int
start_service ()
{
  struct worker_context *start_ctx[DEFAULT_THRD_NUM];
  struct timeval tv = { sco.iMonitorInterval, 0 };
  int nfd, err, i = 0;

#ifdef WINDOWS
  wsa_initialize ();
#endif

  nfd = bind_socket (sco.iInternal_port);
  if (nfd < 0)
  {
    fprintf(stderr, "Manager server couldn't use the cm-api internal port %d, because it seems to be used by other process.\n", sco.iInternal_port); 
    return -1;
  }

  for (i = 0; i < DEFAULT_THRD_NUM; i++)
    {
      start_ctx[i] = (struct worker_context *) malloc (sizeof (struct worker_context));
      start_ctx[i]->base = event_base_new ();
      start_ctx[i]->first = true;
      if (i > 1) /* DEFAULT_THRD_NUM - 1 for request handler */
      {
          start_ctx[i]->timer = event_new (start_ctx[i]->base, -1, EV_PERSIST,
                                           cub_timeout_cb, (void *) start_ctx[i]);
          start_ctx[i]->httpd = evhttp_new (start_ctx[i]->base);
          err = evhttp_accept_socket (start_ctx[i]->httpd, nfd);
          if (err != 0)
    	    continue;
          evhttp_set_timeout (start_ctx[i]->httpd, DEFAULT_HTTP_TIMEOUT);
          evhttp_set_cb (start_ctx[i]->httpd, "/cci",
                                  cub_generic_request_handler, (void *) "cci");
          evhttp_set_cb (start_ctx[i]->httpd, "/cm_api",
    		                  cub_generic_request_handler, (void *) "cm_api");
          evhttp_set_cb (start_ctx[i]->httpd, "/ctrl",
                                  cub_ctrl_request_handler, NULL);
          evhttp_set_cb (start_ctx[i]->httpd, "/upload",
                                  cub_post_request_handler, NULL);
      }
      else if (i == 1){ /* index 1 for monitoring stat job */
        start_ctx[i]->timer = event_new (start_ctx[i]->base, -1, EV_TIMEOUT,
                                                           cub_monitor_stat_cb, (void *) start_ctx[i]);
        start_ctx[i]->httpd = NULL;
      }
      else /* index 0 for auto jobs */
     {
        start_ctx[i]->timer = event_new (start_ctx[i]->base, -1, EV_TIMEOUT,
                                                             cub_auto_jobs_cb, (void *) start_ctx[i]);
        start_ctx[i]->httpd = NULL;
     }
      if (1 == i) {
        struct timeval stat_tv = { 1, 0 };
        evtimer_add (start_ctx[i]->timer, &stat_tv);
      }
      else {
          evtimer_add (start_ctx[i]->timer, &tv);
      }
#ifdef WINDOWS
      start_ctx[i]->ths =
	CreateThread (NULL, 0, dispatch_thread, start_ctx[i], 0, NULL);
#else
      pthread_create (&(start_ctx[i]->ths), NULL, dispatch_thread,
		      (void *) start_ctx[i]);
#endif
    }

  for (i = 0; i < DEFAULT_THRD_NUM; i++)
    {
#ifdef WINDOWS
      WaitForSingleObject (start_ctx[i]->ths, INFINITE);
#else
      pthread_join (start_ctx[i]->ths, NULL);
#endif
      free (start_ctx[i]);
    }

#ifdef WINDOWS
  WSACleanup ();
#endif

  return 0;
}

void
stop_service ()
{
  FILE *pidfile;
  int pidnum;
  char dbmt_file[512];
  conf_get_dbmt_file (FID_CMSERVER_PID, dbmt_file);
  if (access (dbmt_file, F_OK) < 0)
    {
      fprintf (stderr, "Error : Can not stop. Server not running.\n");
    }
  else
    {
      pidfile = fopen (dbmt_file, "rt");
      if (pidfile != NULL)
	{
	  fscanf (pidfile, "%d", &pidnum);
	  fclose (pidfile);
	}

      if ((pidfile == NULL) || ((kill (pidnum, SIGTERM)) < 0))
	{
	  fprintf (stderr, "Error : Failed to stop the server.\n");
	}
    }

  return;
}

int
get_processid ()
{
  FILE *pidfile;
  int pidnum;
  char tmpfile[512];

  conf_get_dbmt_file (FID_CMSERVER_PID, tmpfile);

  if (access (tmpfile, F_OK) < 0)
    return 0;

  pidfile = fopen (tmpfile, "rt");
  if (pidfile != NULL)
    {
      fscanf (pidfile, "%d", &pidnum);
      fclose (pidfile);
    }
  if (pidfile == NULL || ((kill (pidnum, 0) < 0) && (errno == ESRCH)))
    {
      unlink (tmpfile);
      return 0;
    }

  return pidnum;
}

#if defined(WINDOWS)
int
CtrlHandler (DWORD fdwCtrlType)
{
  switch (fdwCtrlType)
    {
    case CTRL_C_EVENT:
    case CTRL_CLOSE_EVENT:
    case CTRL_SHUTDOWN_EVENT:
      cub_loop_flag = 0;
      return FALSE;
    case CTRL_BREAK_EVENT:
    case CTRL_LOGOFF_EVENT:
    default:
      return FALSE;
    }
}
#else
static void
term_handler (int signo)
{
  cub_loop_flag = 0;
}
#endif

static void
print_usage (char *pname)
{
  printf ("Usage: %s [command]\n", pname);
  printf ("commands are :\n");
  printf ("   start   -  start the server\n");
  printf ("   stop    -  stop the server\n");
}

int
main (int argc, char **argv)
{
  char dbmt_file[256];
  int pidnum = 0;
  cub_cm_init_env ();
  if (argc >= 2)
    {
      if (strcmp (argv[1], "stop") == 0)
	{
	  stop_service ();
	  exit (0);
	}
      else if (strcmp (argv[1], "--version") == 0)
	{
	  fprintf (stdout, "CUBRID Manager Server ver : %s\n",
		   makestring (BUILD_NUMBER));
	  exit (0);
	}
      else if (strcmp (argv[1], "getpid") == 0)
	{
	  pidnum = get_processid ();
	  if (pidnum)
	    {
	      fprintf (stdout, "%d\n", pidnum);
	    }
	  exit (0);
	}
      else if (strcmp (argv[1], "start") != 0)
	{
	  fprintf (stderr, "Error : Invalid command - %s\n", argv[1]);
	  print_usage (argv[0]);
	  exit (1);
	}
    }

  if ((pidnum = get_processid ()) > 0)
    {
      fprintf (stdout, "Server [pid=%d] already running\n", pidnum);
      return 0;
    }

  ut_daemon_start ();
#if defined(WINDOWS)
  SetConsoleCtrlHandler ((PHANDLER_ROUTINE) CtrlHandler, TRUE);
#else
  signal (SIGINT, term_handler);
  signal (SIGTERM, term_handler);
#endif

  if (ut_write_pid (conf_get_dbmt_file (FID_CMSERVER_PID, dbmt_file)) < 0)
    {
      fprintf (stderr, "fail to write pid file(%s)\n", dbmt_file);
      exit (1);
    }

  start_service ();
  unlink (dbmt_file);
  return 0;
}
