/*
 * Copyright (C) 2008 Search Solution Corporation. All rights reserved by Search Solution.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */


/*
 * cm_jobfile.c -
 */

#ident "$Id$"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#if defined(WINDOWS)
#include <io.h>
#include <process.h>
#include <winsock2.h>
#include <Tlhelp32.h>
#else
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <sys/wait.h>
#include <netinet/in.h>
#endif

#include "cm_porting.h"
#include "cm_server_util.h"

#if defined(WINDOWS)
#include "cm_win_wsa.h"
#endif

#include "cm_compress.h"

#define BUF_SIZE 8192
#define MAX_LINE 1024
#define SUCC_MSG "success"

#define ERR_SUCCESS     0
#define ERR_FAILURE     -1
#define ERR_CONN        -100
#define ERR_WRITE       -101

#define START_PORT      21000
#define END_PORT        22000

/*upload file flag*/
#define FILE_NORM   0
#define FILE_ZIP        1

#if defined(WINDOWS)
#define ftruncate(fd, length)  _chsize (fd, length)
#endif

#define LOG

typedef struct
{
  char *filepath;
  int content_length;
  int zipfile;
} FILE_TRANS_HEAD;

static void receive_files (int sockfd);
static int receive_a_file (int sockfd, nvplist * req, nvplist * res,
			   char *errmsg, int errlen);
static void make_response (nvplist * res, const char *task,
			   const char *errmsg);
static void get_folder_path (const char *filepath, char *buf, int buf_size);
static int create_folder_if_not_exist (const char *filepath, char *errstr,
				       int errlen);
static int get_param_from_req (nvplist * req, FILE_TRANS_HEAD * fth);

int
main (void)
{
  int listenfd, connfd;
  T_SOCKLEN clilen;
  struct sockaddr_in cliaddr, servaddr;
  int on = 1;

  fd_set rfd;
  int nfound = 0;
  int maxfd = 0;
  int nsec = 120;		/* if after nsec seconds, ftclient still not connect, ftprocess will exit. */
  struct timeval timeval_out;

  int port;

  timeval_out.tv_sec = 120;
  timeval_out.tv_usec = 0;

#if defined (WINDOWS)
  if (wsa_initialize () < 0)
    {
      fprintf (stderr, "socket: wsa initialize failure.\n");
      exit (1);
    }
#endif

  if ((listenfd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
      fprintf (stderr, "socket:%s", strerror (errno));
      exit (1);
    }

  setsockopt (listenfd, IPPROTO_TCP, TCP_NODELAY, (char *) &on, sizeof (on));

  memset (&servaddr, 0, sizeof (servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = htonl (INADDR_ANY);

  for (port = START_PORT; port < END_PORT; port++)
    {
      servaddr.sin_port = htons (port);
      if (bind
	  (listenfd, (struct sockaddr *) &servaddr, sizeof (servaddr)) == 0)
	{
	  break;
	}
    }

  if (port == END_PORT)
    {
      fprintf (stderr, "failed to find an available port");
      exit (1);
    }

  if (listen (listenfd, 1024) < 0)
    {
      fprintf (stderr, "listen:%s", strerror (errno));
      exit (1);
    }

  fprintf (stdout, "port:%d", port);	/* notify the ftproc port to cub_job */
  fclose (stdout);

  fprintf (stderr, SUCC_MSG);	/* tell cub_job not to wait */
  fclose (stderr);

  FD_ZERO (&rfd);
  FD_SET (listenfd, (fd_set *) & rfd);
  maxfd = (int) listenfd + 1;

  nfound = select (maxfd, &rfd, NULL, NULL, &timeval_out);
  if (nfound < 0)
    {
      perror ("out of time.");
      exit (1);
    }

  if (!FD_ISSET (listenfd, (fd_set *) & rfd))
    {
      perror ("select error");
      exit (1);
    }

  clilen = sizeof (cliaddr);
  if ((connfd = accept (listenfd, (struct sockaddr *) &cliaddr, &clilen)) < 0)
    {
      perror ("accept");
      exit (1);
    }

  receive_files (connfd);	/* start receive files. */

  CLOSE_SOCKET (connfd);
  CLOSE_SOCKET (listenfd);
  return 0;
}

/*
 * Private functions.
 */
static mz_bool 
unzip (const char *zip_file, const char *unzip_dir) 
{
  mz_uint i;
  mz_bool status;
  mz_uint num_files;
  mz_zip_archive zip_archive;
  char unzip_file[MAX_LINE];
  if (NULL == zip_file || NULL == unzip_dir)
    
    {
      return MZ_FALSE;
    }
  
    // Now try to open the archive.
    memset (&zip_archive, 0, sizeof (zip_archive));
  status = mz_zip_reader_init_file (&zip_archive, zip_file, 0);
  if (!status)
    
    {
      return MZ_FALSE;
    }
  num_files = mz_zip_reader_get_num_files (&zip_archive);
  
    // Get and print information about each file in the archive.
    for (i = 0; i < num_files; i++)
    
    {
      mz_zip_archive_file_stat file_stat;
      status = mz_zip_reader_file_stat (&zip_archive, i, &file_stat);
      if (!status)
	
	{
	  mz_zip_reader_end (&zip_archive);
	  return MZ_FALSE;
	}
      snprintf (unzip_file, MAX_LINE, "%s/%s", unzip_dir,
		  file_stat.m_filename);
      status =
	mz_zip_reader_extract_file_to_file (&zip_archive,
					    file_stat.m_filename, unzip_file,
					    0);
      if (!status)
	
	{
	  mz_zip_reader_end (&zip_archive);
	  return MZ_FALSE;
	}
    }
  mz_zip_reader_end (&zip_archive);
  return MZ_TRUE;
}



static int
receive_a_file (int sockfd, nvplist * req, nvplist * res, char *errmsg,
		int errlen)
{
  int nbytes;
  int ncontent;
  int retval = ERR_SUCCESS;
  int mode = 0644;
  FILE *fp = NULL;

  FILE_TRANS_HEAD fth;
  char buf[BUF_SIZE + 1];
  char unzip_dir[MAX_LINE];

  memset (&fth, 0, sizeof (fth));

  /* get filepath & content length from request. */
  if (get_param_from_req (req, &fth) < 0)
    {
      strcpy_limit (errmsg, "Parameter error", errlen);
      fprintf (stderr, "ERROR: %s.\n", errmsg);
      return ERR_FAILURE;
    }

  ncontent = fth.content_length;

  /* check folder. */
  if (create_folder_if_not_exist (fth.filepath, errmsg, errlen) < 0)
    {
      fprintf (stderr, "ERROR: %s.\n", errmsg);
      return ERR_FAILURE;
    }

  /* all paramters and environment are checked, now download the file. */
  if ((fp = fopen (fth.filepath, "wb")) == NULL)
    {
      strcpy_limit (errmsg, strerror (errno), errlen);
      fprintf (stderr, "ERROR: %s.\n", errmsg);
      return ERR_FAILURE;
    }

  ut_send_response (sockfd, res);

  for (; ncontent > 0;)
    {
      if ((nbytes = read_from_socket (sockfd, buf, BUF_SIZE)) > 0)
	{			/* read ncontent at most */
	  if ((int) fwrite (buf, 1, nbytes, fp) != nbytes)
	    {
	      strcpy_limit (errmsg, "failed to write to the target file",
			    errlen);
	      retval = ERR_WRITE;
	      goto msg_to_ftclient;
	    }
	  ncontent -= nbytes;
	}
      else if (nbytes == 0)
	{
	  strcpy_limit (errmsg, "client closed prematurely!", errlen);
	  retval = ERR_CONN;
	  goto msg_to_ftclient;
	}
      else
	{			/* time out */
	  continue;
	}
    }

  fclose (fp);

  if (FILE_ZIP == fth.zipfile)
    {
      get_folder_path (fth.filepath, unzip_dir, MAX_LINE);
      if (!unzip (fth.filepath, unzip_dir))
	{
	  unlink (fth.filepath);
	  strcpy_limit (errmsg, "unzip file error!", errlen);
	  fprintf (stderr, "ERROR: %s.\n", errmsg);
	  return ERR_FAILURE;
	}
      unlink (fth.filepath);
    }

  return ERR_SUCCESS;

msg_to_ftclient:
  fprintf (stderr, "ERROR: %s.\n", errmsg);
  fclose (fp);
  return retval;
}

static void
receive_files (int sockfd)
{
  nvplist *req, *res;

  req = nv_create (3, NULL, "\n", ":", "\n");
  res = nv_create (3, NULL, "\n", ":", "\n");

  for (;;)
    {
      char errstr[MAX_LINE] = "none";

      if (ut_receive_request (sockfd, req) < 0)
	{
	  /* just stop the process when connection is closed. */
	  break;
	}

      make_response (res, "recvfile", "none");

      if (receive_a_file (sockfd, req, res, errstr, sizeof (errstr)) ==
	  ERR_CONN)
	{
	  /* end the process if problem occur when transferring files. */
	  break;
	}

      nv_update_val (res, "note", errstr);
      ut_send_response (sockfd, res);
      nv_reset_nvp (req);
      nv_reset_nvp (res);
    }

  nv_destroy (req);
  nv_destroy (res);
  return;
}

static void
make_response (nvplist * res, const char *task, const char *errmsg)
{
  nv_add_nvp (res, "task", task);
  nv_add_nvp (res, "status",
	      ((strcmp (errmsg, "none") == 0) ? SUCC_MSG : "failure"));
  nv_add_nvp (res, "note", errmsg);
  return;
}

static void
get_folder_path (const char *filepath, char *buf, int buf_size)
{
  char *end = NULL;

  strcpy_limit (buf, filepath, buf_size);
#if defined(WINDOWS)
  unix_style_path (buf);
#endif
  end = strrchr (buf, '/');
  if (end != NULL)
    *end = '\0';

  return;
}

static int
create_folder_if_not_exist (const char *filepath, char *errstr, int errlen)
{
  char destfolder[PATH_MAX];

  if (filepath == NULL)
    {
      snprintf (errstr, errlen - 1, "filepath is NULL");
      return -1;
    }

  /* check the exist of parent folder. */
  get_folder_path (filepath, destfolder, PATH_MAX);
  if (access (destfolder, F_OK) < 0)
    {
      if (uCreateDir (destfolder) != ERR_NO_ERROR)
	{
	  snprintf (errstr, errlen - 1,
		    "can't create destination folder: %s", destfolder);
	  return -1;
	}
    }

  /* check permission of the directory */
  if (access (destfolder, W_OK) < 0)
    {
      snprintf (errstr, errlen - 1, "No write permission for %s", destfolder);
      return -1;
    }

  return 0;
}

static int
get_param_from_req (nvplist * req, FILE_TRANS_HEAD * fth)
{
  char *content_length;

  if ((fth->filepath = nv_get_val (req, "filepath")) == NULL)
    {
      return -1;
    }

  if ((content_length = nv_get_val (req, "contentlength")) == NULL)
    {
      return -1;
    }
  fth->content_length = atoi (content_length);

  if ((content_length = nv_get_val (req, "zipfile")) != NULL
      && !strcmp (content_length, "y"))
    
    {
      fth->zipfile = FILE_ZIP;
    }

  return 0;
}
