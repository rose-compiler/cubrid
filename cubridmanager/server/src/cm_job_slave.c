/*
 * Copyright (C) 2008 Search Solution Corporation. All rights reserved by Search Solution.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */


/*
 * fserver_slave.c -
 */

#ident "$Id$"

#include "config.h"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#if defined(WINDOWS)
#include <io.h>
#include <process.h>
#include <sys/timeb.h>
#else
#include <unistd.h>
#include <sys/time.h>
#endif

#include "cm_porting.h"
#include "cm_server_util.h"
#include "cm_stat.h"
#include "cm_dep.h"
#include "cm_job_task.h"
#include "cm_config.h"
#include "cm_text_encryption.h"
#include "assert.h"
#include "cm_connect_info.h"

#if defined(WINDOWS)
#include "cm_win_wsa.h"
#endif

#ifdef	_DEBUG_
#include "deb.h"
#endif
//char cubrid_charset[PATH_MAX] = "";
static int ut_process_request (nvplist * req, nvplist * res);

T_EMGR_VERSION CLIENT_VERSION;

int
main (int argc, char *argv[])
{
  char *in_file, *out_file;
  nvplist *cli_request, *cli_response;
  char cubrid_err_log_file[256];
  char cubrid_err_log_env[256 + 32];
  char * charset = NULL;

  if (argc < 3)
    exit (0);

#if defined(WINDOWS)
  wsa_initialize ();
#endif

  in_file = argv[1];
  out_file = argv[2];

  sys_config_init ();
  uReadEnvVariables (argv[0], stdout);
  uReadSystemConfig ();

  /* From now on, interpret all the message as 'en_US' type.
   * In other language setting, it may corrupt.
   */
  putenv ((char *) "CUBRID_LANG=en_US");	/* set as default language type */
  putenv ((char *) "CUBRID_MSG_LANG=en_US");  /* set as default language type */
  //putenv ((char *) "CUBRID_CHARSET=en_US");	/* set as default language type */
  /*charset = getenv("CUBRID_CHARSET");
  if (charset != NULL)
    {
        snprintf(cubrid_charset, PATH_MAX, "CUBRID_CHARSET=%s", charset);
    }
  else
    {
        snprintf(cubrid_charset, PATH_MAX, "CUBRID_CHARSET=en_US");
        putenv(cubrid_charset);
    }
  */

  sprintf (cubrid_err_log_file, "%s/cmclt.%d.err", sco.dbmt_tmp_dir,
	   (int) getpid ());
  sprintf (cubrid_err_log_env, "CUBRID_ERROR_LOG=%s", cubrid_err_log_file);
  putenv (cubrid_err_log_env);

  cli_request = nv_create (5, NULL, "\n", ":", "\n");
  cli_response = nv_create (5, NULL, "\n", ":", "\n");
  if ((cli_request != NULL) && (cli_response != NULL))
    {
      nv_readfrom (cli_request, in_file);
      ut_process_request (cli_request, cli_response);
      nv_writeto (cli_response, out_file);
    }

  nv_destroy (cli_request);
  nv_destroy (cli_response);

  unlink (cubrid_err_log_file);

  return 0;
}

static int
ut_process_request (nvplist * req, nvplist * res)
{
    int task_code;
    int retval = ERR_NO_ERROR;
    char *dbname, *task, *private_key, *charset = NULL;
    static char charsetenv[PATH_MAX];
    char dbid[32];
    char dbpasswd[80];
    T_TASK_FUNC task_func;
    char access_log_flag;
    char _dbmt_error[DBMT_ERROR_MSG_SIZE];
    int major_ver, minor_ver;
    char *cli_ver;

    /* variables for caculating the running time of cub_job. */
    int elapsed_msec = 0;
    struct timeval task_begin, task_end;
    char elapsed_time_str[20];


    memset (_dbmt_error, 0, sizeof (_dbmt_error));

    task = nv_get_val (req, "task");
    dbname = nv_get_val (req, "dbname");
    private_key = nv_get_val (req, "#private_key");
    charset = nv_get_val (req, "charset");

    task_code = ut_get_task_info (task, &access_log_flag, &task_func, NULL);
    switch (task_code)
    {
        /* case TS_ANALYZECASLOG: */
        case TS_GET_DIAGDATA:
            nv_reset_nvp (res);
            nv_init (res, 5, NULL, "\n", ":DIAG_DEL:", "END__DIAGDATA\n");
            break;
    }

    /* insert task,status,note to the front of response */
    nv_add_nvp (res, "task", task);
    nv_add_nvp (res, "status", "none");
    nv_add_nvp (res, "note", "none");

/*    if (private_key == NULL)
    {
        sprintf(log_tmp, "%d: task = %s in job_slave, before validate token.", __LINE__, task); 
        ut_access_log(req, log_tmp); 

        if (ut_validate_token (req) == 0)
        {
            retval = ERR_INVALID_TOKEN;
            uGenerateStatus (req, res, retval, _dbmt_error);
            //ut_access_log(req, "token invalidate"); 
            return 0;
        }

        sprintf(log_tmp, "%d: task = %s in job_slave, before validate auth.", __LINE__, task); 
        ut_access_log(req, log_tmp); 

        if (ut_validate_auth(req) == 0) 
        {
            retval = ERR_PERMISSION; 
            strncpy(_dbmt_error, task, DBMT_ERROR_MSG_SIZE); 
            uGenerateStatus (req, res, retval, _dbmt_error); 
            ut_access_log(req, "auth invalidate"); 
            return 0; 
        }
    } */   

    /* if database name is specified */
    if (dbname)
    {
        _ut_get_dbaccess (req, dbid, dbpasswd);
        nv_add_nvp (req, "_DBID", dbid);
        nv_add_nvp (req, "_DBPASSWD", dbpasswd);
        nv_add_nvp (req, "_DBNAME", dbname);
    }

    /* set CLIENT_VERSION */
    cli_ver = nv_get_val (req, "_CLIENT_VERSION");

    if (cli_ver == NULL)
        cli_ver = strdup ("1.0");

    make_version_info (cli_ver, &major_ver, &minor_ver);
    CLIENT_VERSION = EMGR_MAKE_VER (major_ver, minor_ver);	/* global variable */

    sprintf (_dbmt_error, "?");	/* prevent to have null string */
    if (task_code == TS_UNDEFINED)
    {
        if (task != NULL)
        {
            strcpy (_dbmt_error, task);
        }
        retval = ERR_UNDEFINED_TASK;

    }
    else
    {
        if (access_log_flag)
        {
            ut_access_log (req, NULL);
        }
        /*     if (charset != NULL)
               {
               snprintf(charsetenv, PATH_MAX, "CUBRID_CHARSET=%s", charset);
               putenv(charsetenv);
               }
               */
        /* record the start time of running cub_job */
        gettimeofday (&task_begin, NULL);

        retval = (*task_func) (req, res, _dbmt_error);

        /* record the end time of running cub_job */
        gettimeofday (&task_end, NULL);
        /*    if (charset != NULL)
              {
              putenv(cubrid_charset);
              }
              */        
        /* caculate the running time of cub_job. */
        _ut_timeval_diff (&task_begin, &task_end, &elapsed_msec);

        /* add cub_job running time to response. */
        snprintf (elapsed_time_str, sizeof (elapsed_time_str),
                "%d ms", elapsed_msec);
        nv_add_nvp (res, "__EXEC_TIME", elapsed_time_str);
    }

    uGenerateStatus (req, res, retval, _dbmt_error);

    FREE_MEM (cli_ver);

    return 0;
}


