/*
 * Copyright (C) 2008 Search Solution Corporation. All rights reserved by Search Solution.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */


/*
 * fserver.c -
 */

#ident "$Id$"

#include <stdio.h>
#include <stdlib.h>		/* atoi()    */
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <ctype.h>		/* isalnum() */
#include <sys/stat.h>		/* stat()    */
#include <fcntl.h>

#if defined(WINDOWS)
#include <winsock2.h>
#include <windows.h>
#include <process.h>
#include <Tlhelp32.h>
#include <io.h>
#else
#include <dirent.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>		/* getpid()  */
#include <netinet/in.h>
#include <pthread.h>
#include <netinet/tcp.h>
#endif

#include "cm_porting.h"
#include "cm_config.h"
#include "cm_dep.h"
#include "cm_server_util.h"
#include "cm_text_encryption.h"
#include "cm_config.h"
#include "cm_job_task.h"
#include "cm_stat.h"
#if defined(WINDOWS)
#include "cm_win_wsa.h"
#endif

#ifdef	_DEBUG_
#include "deb.h"
#endif

#define MAX_CLIENT_NUM 128
typedef struct
{
  SOCKET sock_fd;
  time_t last_time;
  char user_id[35];
} T_KEEP_ALIVE_INFO;

typedef struct
{
  SOCKET clt_sock_fd;
  unsigned int req_count;
  T_KEEP_ALIVE_INFO *keep_alive_info;
} T_THR_ARG;

typedef struct
{
  SOCKET sockfd;
  int pid;
  char taskname[512];
} T_THR_CANCEL_ARG;

char g_pidfile_path[1024] = "";

static MUTEX_T mutex_thread_num;
static COND_T cond_thread_num;
static int job_num = 0;

static void service_start (void);
static THREAD_FUNC fserver_slave_thr_f (void *arg);
static THREAD_FUNC mon_cub_auto (void *arg);
static int is_conflict (char *task_entered, char *dbname_entered);
static int is_monitor_task (char *task_entered);
static void print_usage (char *pname);
static void start_fserver (void);
static int fserver_net_init (void);
static int pserver_net_init (void);
static int send_res_file (SOCKET sock_fd, char *filename, char *errmsg,
			  int errmsg_len);
static void make_fail_response (nvplist * res, const char *task,
				const char *errmsg);
static int kill_process_tree (int pid);

static void keep_alive (SOCKET sock_fd);
static void reset_keep_alive_info (T_KEEP_ALIVE_INFO * keep_alive_info);

static SOCKET fserver_sockfd;
static SOCKET pserver_sockfd;

static FILE *start_log_fp;

#if defined(WINDOWS)
int
CtrlHandler (DWORD fdwCtrlType)
{
  switch (fdwCtrlType)
    {
    case CTRL_C_EVENT:
    case CTRL_CLOSE_EVENT:
    case CTRL_SHUTDOWN_EVENT:
      if (strlen (g_pidfile_path) > 0)
	unlink (g_pidfile_path);
      return FALSE;
    case CTRL_BREAK_EVENT:
    case CTRL_LOGOFF_EVENT:
    default:
      return FALSE;
    }
}
#else
static void
term_handler (int signo)
{
  g_pidfile_path[sizeof (g_pidfile_path) - 1] = '\0';
  if (strlen (g_pidfile_path) > 0)
    unlink (g_pidfile_path);
  exit (0);
}
#endif

#if defined(WINDOWS)
static int
kill_process_tree (int pid)
{
  BOOL bContinue;
  HANDLE hSnapshot;
  PROCESSENTRY32 procEntry;

  hSnapshot = CreateToolhelp32Snapshot (TH32CS_SNAPPROCESS, 0);
  bContinue = Process32First (hSnapshot, &procEntry);

  while (bContinue)
    {
      if (pid == procEntry.th32ParentProcessID)
	{
	  kill_process_tree (procEntry.th32ProcessID);
	}

      bContinue = Process32Next (hSnapshot, &procEntry);
    }

  /*
   * Try to kill the process, and no need to check the return value of kill.
   */
  kill (pid, SIGTERM);

  return 0;
}
#else
static int
kill_process_tree (int pid)
{
  int retval = 0;
  int argc = 0;
  const char *argv[8];
  char ps_tree_file[PATH_MAX];
  char buf[1024];
  char tok[32];
  char ppid_str[32];
  FILE *fp;

  snprintf (ppid_str, sizeof (ppid_str) - 1, "%d", pid);

  argv[argc++] = "/bin/ps";
  argv[argc++] = "--ppid";
  argv[argc++] = ppid_str;
  argv[argc++] = NULL;

  snprintf (ps_tree_file, sizeof (ps_tree_file) - 1, "%s/DBMS_task_cancel.%d",
	    sco.dbmt_tmp_dir, pid);

  retval = run_child (argv, 1, NULL, ps_tree_file, NULL, NULL);	/* ps --ppid */
  if (retval < 0)
    return -1;

  fp = fopen (ps_tree_file, "rt");
  if (fp == NULL)
    return -1;

  /* kill children. */
  while (fgets (buf, sizeof (buf), fp))
    {
      /* tok[0] -> pid, tok[1] -> child_pid. */
      if (sscanf (buf, "%30s", tok) != 1)
	continue;

      if (isdigit (tok[0]))
	{
	  /* kill the child process. */
	  kill_process_tree (atoi (tok));
	}
    }

  /* kill itself. */
  kill (pid, SIGKILL);

  fclose (fp);
  unlink (ps_tree_file);

  return 0;
}
#endif


static THREAD_FUNC
fserver_slave_thr_f (void *arg)
{
  int one = 1;
  char *task = NULL;
  char *user_id = NULL;
  char cmd_name[512];
  char req_filename[512];
  char res_filename[512];
  char errmsg[1024];
  const char *argv[4];
  nvplist *cli_request = NULL;
  nvplist *cli_response = NULL;
  T_SOCKLEN slen;
  struct sockaddr_in temp_addr;
  int pid = -1;

  unsigned int req_count = ((T_THR_ARG *) arg)->req_count;
  SOCKET clt_sock_fd = ((T_THR_ARG *) arg)->clt_sock_fd;
  T_KEEP_ALIVE_INFO *keep_alive_info = NULL;

  fd_set read_mask;
  int nfound;
  int maxfd;
  struct timeval timeout_val;
  char c;

  char _dbmt_error[DBMT_ERROR_MSG_SIZE];
  char log_tmp[1024];

  cli_request = nv_create (5, NULL, "\n", ":", "\n");
  cli_response = nv_create (5, NULL, "\n", ":", "\n");

  if ((cli_request == NULL) || (cli_response == NULL))
    {
      goto sock_close_return;
    }

  setsockopt (clt_sock_fd, IPPROTO_TCP, TCP_NODELAY, (char *) &one,
	      sizeof (one));

  /* receive request from client socket. */
  if (ut_receive_request (clt_sock_fd, cli_request) < 0)
    {
      strcpy_limit (errmsg, "The task format or encrypt is wrong.",
		    sizeof (errmsg));
      goto err_return;
    }

  if ((task = nv_get_val (cli_request, "task")) == NULL)
    {
      strcpy_limit (errmsg, "There is no task node in request message.",
		    sizeof (errmsg));
      goto err_return;
    }

  /* check the conflict of the task. */
  /* don't check the conflict of a monitoring task. */
  if (is_monitor_task (task) == 0)
    {
      char *dbname = nv_get_val (cli_request, "dbname");

      /* conflict resolution */
      if (is_conflict (task, dbname))
	{
	  strcpy_limit (errmsg,
			"Cannot execute the current operation because the previous operation is already running.",
			sizeof (errmsg));
	  goto err_return;
	}
    }

  /* add server-specific information to the request */
  nv_add_nvp_int (cli_request, "_STAMP", req_count);
  nv_add_nvp (cli_request, "_SVRTYPE", "fsvr");
  nv_add_nvp (cli_request, "_PROGNAME", sco.szProgname);
  slen = sizeof (temp_addr);
  if (getpeername (clt_sock_fd, (struct sockaddr *) &temp_addr, &slen) == 0)
    {
      nv_add_nvp (cli_request, "_CLIENTIP", inet_ntoa (temp_addr.sin_addr));
      nv_add_nvp_int (cli_request, "_CLIENTPORT", ntohs (temp_addr.sin_port));
    }
  else
    {
      goto sock_close_return;
    }

  /* Prepare parameters for running cub_job task. */
  sprintf (req_filename, "%s/DBMT_comm_%d.%d.req", sco.dbmt_tmp_dir,
	   (int) getpid (), req_count);
  sprintf (res_filename, "%s/DBMT_comm_%d.%d.res", sco.dbmt_tmp_dir,
	   (int) getpid (), req_count);

  if (!strcmp (task, "login"))
    {
      int retval;
      user_id = nv_get_val (cli_request, "id");
      if (user_id != NULL)
	{
	  int i;
	  int new_client = -1;

	  keep_alive_info = ((T_THR_ARG *) arg)->keep_alive_info;
	  for (i = 0; i < MAX_CLIENT_NUM; ++i)
	    {
	      if (new_client == -1
		  && IS_INVALID_SOCKET (keep_alive_info[i].sock_fd))
		{
		  new_client = i;
		  continue;
		}
	      if (!strcmp (user_id, keep_alive_info[i].user_id))
		{
		  break;
		}
	    }

	  if (i != MAX_CLIENT_NUM)
	    {
	      CLOSE_SOCKET (keep_alive_info[i].sock_fd);
	      keep_alive_info[i].sock_fd = clt_sock_fd;
	      keep_alive_info[i].last_time = time (NULL);
	    }
	  else
	    {
	      keep_alive_info[new_client].sock_fd = clt_sock_fd;
	      strncpy (keep_alive_info[new_client].user_id, user_id, 34);
	      keep_alive_info[new_client].last_time = time (NULL);
	    }
	}

      nv_add_nvp (cli_response, "task", "login");
      nv_add_nvp (cli_response, "status", "none");
      nv_add_nvp (cli_response, "note", "none");
      retval = ts_login (cli_request, cli_response, _dbmt_error);
      uGenerateStatus (cli_request, cli_response, retval, _dbmt_error);
      nv_writeto (cli_response, res_filename);

      if (send_res_file (clt_sock_fd, res_filename, errmsg, sizeof (errmsg)) <
	  0)
	{
	  ut_access_log (cli_request, "send res failed.");
	  unlink (res_filename);
	  goto err_return;
	}
      unlink (res_filename);

      goto dont_close_sock_return;
    }
  else if (!strcmp (task, "getcmsenv"))
    {
      int retval;
      nv_add_nvp (cli_response, "task", "getcmsenv");
      nv_add_nvp (cli_response, "status", "none");
      nv_add_nvp (cli_response, "note", "none");
      retval = ts_get_cms_env (cli_request, cli_response, _dbmt_error);
      uGenerateStatus (cli_request, cli_response, retval, _dbmt_error);
      nv_writeto (cli_response, res_filename);

      goto normally_end;
    }
  else
    {
      if (!ut_validate_token (cli_request))
	{
	  snprintf (_dbmt_error, DBMT_ERROR_MSG_SIZE,
		    "Request is rejected due to invalid token. Please reconnect.");
	  make_fail_response (cli_response, task, _dbmt_error);
	  nv_writeto (cli_response, res_filename);

	  goto normally_end;
	}

      if (!ut_validate_auth (cli_request))
	{
	  snprintf (_dbmt_error, DBMT_ERROR_MSG_SIZE,
		    "The user don't have authority to execute the task: %s.",
		    task);
	  make_fail_response (cli_response, task, _dbmt_error);
	  nv_writeto (cli_response, res_filename);

	  goto normally_end;
	}
    }

  nv_writeto (cli_request, req_filename);

#if !defined (DO_NOT_USE_CUBRIDENV)
  sprintf (cmd_name, "%s/bin/cub_job%s", sco.szCubrid, DBMT_EXE_EXT);
#else
  sprintf (cmd_name, "%s/cub_job%s", CUBRID_BINDIR, DBMT_EXE_EXT);
#endif
  argv[0] = cmd_name;
  argv[1] = req_filename;
  argv[2] = res_filename;
  argv[3] = NULL;

  sprintf (log_tmp,
	   "%d: task= %s, in job_server thread, ready to start cub_job.",
	   __LINE__, task);
  ut_access_log (cli_request, log_tmp);

  /* run cub_job task. */
  pid = run_child (argv, 0, NULL, NULL, NULL, NULL);	/* cub_job */

  /* check the return value of the cub_job task. */
  if (pid >= 0)
    {
      /* wait for the end of cub_job or task cancel signal. */
    again:
      timeout_val.tv_sec = 0;
      timeout_val.tv_usec = 100000;	/* 100 ms */

      FD_ZERO (&read_mask);
      FD_SET (clt_sock_fd, (fd_set *) & read_mask);
      maxfd = (int) clt_sock_fd + 1;
      nfound =
	select (maxfd, &read_mask, (fd_set *) 0, (fd_set *) 0, &timeout_val);
      if (nfound < 0)
	{
	  goto again;		/* unexpect error */
	}
      else if (nfound == 0)	/* time out */
	{
	  if (kill (pid, 0) == 0)	/* cub_job still exists, continue waiting */
	    goto again;

	  ut_access_log (cli_request, "cub_job is stopped.");
	}
      else			/* there's something readable, we take that for a job cancel signal */
	{
	  while (recv (clt_sock_fd, &c, 1, 0) > 0);	/* consume all readable things, we're expecting client calls close() */
	  kill (pid, SIGTERM);	/* kill cub_job */
	  unlink (req_filename);
	  unlink (res_filename);
	  goto sock_close_return;
	}

    normally_end:
      /* cub_job terminated normally */
      unlink (req_filename);
      if (send_res_file (clt_sock_fd, res_filename, errmsg, sizeof (errmsg)) <
	  0)
	{
	  ut_access_log (cli_request, "send res failed.");
	  unlink (res_filename);
	  goto err_return;
	}
      unlink (res_filename);
      goto sock_close_return;
    }
  else
    {
      /* if cub_job fail, send fail response to client. */
      char buf[1024];

      sprintf (buf, "CreateProcess error : %s", cmd_name);
      strcpy_limit (errmsg, buf, sizeof (errmsg));
      goto err_return;
    }

err_return:
  make_fail_response (cli_response, task, errmsg);
  ut_send_response (clt_sock_fd, cli_response);

sock_close_return:
  CLOSE_SOCKET (clt_sock_fd);

dont_close_sock_return:
  nv_destroy (cli_request);
  nv_destroy (cli_response);
  free ((T_THR_ARG *) arg);

  /* release semaphore */
  MUTEX_LOCK (mutex_thread_num);
  job_num--;
  COND_SIGNAL (cond_thread_num);
  MUTEX_UNLOCK (mutex_thread_num);

#if defined(WINDOWS)
  return;
#else
  return NULL;
#endif
}

typedef struct
{
  short run_task;
  char check_dbname;
  char check_task;
  short conflict_task[6];
} T_CONFLICT_TABLE;

static const T_CONFLICT_TABLE cft_table[] = {
  /* Check dbname only for all tasks. */
  {TS_CREATEDB, 1, 0, {-1}},
  {TS_DELETEDB, 1, 0, {-1}},
  {TS_RENAMEDB, 1, 0, {-1}},
  {TS_STARTDB, 1, 0, {-1}},
  {TS_STOPDB, 1, 0, {-1}},
  {TS_COPYDB, 1, 0, {-1}},
  {TS_OPTIMIZEDB, 1, 0, {-1}},
  {TS_CHECKDB, 1, 0, {-1}},
  {TS_COMPACTDB, 1, 0, {-1}},
  {TS_BACKUPDB, 1, 0, {-1}},
  {TS_ADDVOLDB, 1, 0, {-1}},
  {TS_UNLOADDB, 1, 0, {-1}},
  {TS_LOADDB, 1, 0, {-1}},
  {TS_RESTOREDB, 1, 0, {-1}},

  /* Check only the task name.                    */
  /* nlucene service start should be sequential.  */
  {TS_NLUCENE_BLOC_START, 0, 1, {TS_NLUCENE_BLOC_START, -1}},
  {TS_NLUCENE_ADMIN_START, 0, 1, {TS_NLUCENE_ADMIN_START, -1}},
  {TS_GET_NLUCENE_INSTALL_PATH, 0, 1,
   {TS_GET_NLUCENE_INSTALL_PATH, TS_SET_NLUCENE_INSTALL_PATH, -1}},
  {TS_SET_NLUCENE_INSTALL_PATH, 0, 1,
   {TS_GET_NLUCENE_INSTALL_PATH, TS_SET_NLUCENE_INSTALL_PATH, -1}},

  /* Dbmt user manamgement should be sequential.  */
  {TS_GETDBMTUSERINFO, 0, 1,
   {TS_GETDBMTUSERINFO, TS_DELETEDBMTUSER, TS_UPDATEDBMTUSER,
    TS_SETDBMTPASSWD, TS_ADDDBMTUSER, -1}},
  {TS_DELETEDBMTUSER, 0, 1,
   {TS_GETDBMTUSERINFO, TS_DELETEDBMTUSER, TS_UPDATEDBMTUSER,
    TS_SETDBMTPASSWD, TS_ADDDBMTUSER, -1}},
  {TS_UPDATEDBMTUSER, 0, 1,
   {TS_GETDBMTUSERINFO, TS_DELETEDBMTUSER, TS_UPDATEDBMTUSER,
    TS_SETDBMTPASSWD, TS_ADDDBMTUSER, -1}},
  {TS_SETDBMTPASSWD, 0, 1,
   {TS_GETDBMTUSERINFO, TS_DELETEDBMTUSER, TS_UPDATEDBMTUSER,
    TS_SETDBMTPASSWD, TS_ADDDBMTUSER, -1}},
  {TS_ADDDBMTUSER, 0, 1,
   {TS_GETDBMTUSERINFO, TS_DELETEDBMTUSER, TS_UPDATEDBMTUSER,
    TS_SETDBMTPASSWD, TS_ADDDBMTUSER, -1}},
  {TS_GETAUTOADDVOL, 0, 1, {TS_SETAUTOADDVOL, -1}},
  {TS_SETAUTOADDVOL, 0, 1, {TS_GETAUTOADDVOL, -1}},

  /* Check both the dbname and task name.    */
  /* System parameter should be sequential.  */
  {TS_SETSYSPARAM, 1, 1, {TS_SETSYSPARAM, TS_GETALLSYSPARAM, -1}},
#if 0
  {TS_SETSYSPARAM2, 1, 1, {TS_SETSYSPARAM, TS_GETALLSYSPARAM, -1}},
  {TS_GETSYSPARAM, 1, 1, {TS_SETSYSPARAM, TS_GETALLSYSPARAM, -1}},
#endif
  {TS_GETALLSYSPARAM, 1, 1, {TS_SETSYSPARAM, TS_GETALLSYSPARAM, -1}},

  /* Only the db modification tasks are prevented, check dbname. */
  {TS_GENERALDBINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DBSPACEINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_USERINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_CREATEUSER, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DELETEUSER, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_UPDATEUSER, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_CLASSINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_CLASS, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_RENAMECLASS, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DROPCLASS, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_CREATECLASS, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_CREATEVCLASS, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_GETLOGINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ADDATTRIBUTE, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DROPATTRIBUTE, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_UPDATEATTRIBUTE, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ADDCONSTRAINT, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DROPCONSTRAINT, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_GETSUPERCLASSESINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ADDRESOLUTION, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DROPRESOLUTION, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ADDMETHOD, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DROPMETHOD, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_UPDATEMETHOD, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ADDMETHODFILE, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DROPMETHODFILE, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ADDQUERYSPEC, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DROPQUERYSPEC, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_CHANGEQUERYSPEC, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_VALIDATEQUERYSPEC, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_VALIDATEVCLASS, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ADDSUPER, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DROPSUPER, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_VIEWLOG, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_GETBACKUPINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ADDBACKUPINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DELETEBACKUPINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_SETBACKUPINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_GETDBERROR, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_REGISTERLOCALDB, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_REMOVELOCALDB, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ADDNEWTRIGGER, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ALTERTRIGGER, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_DROPTRIGGER, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_USER_VERIFY, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_GETTRIGGERINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_GETFILE, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, TS_BACKUPDB, -1}},
  {TS_PARAMDUMP, 1, 1,
   {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_PLANDUMP, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_ROLE_CHANGE, 1, 1,
   {TS_DELETEDB, TS_STARTDB, TS_STOPDB, TS_RENAMEDB, -1}},

#if 0
  {TS_POPSPACEINFO, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
#endif
#if 0
  {TS_STARTINFO, 0, 0, {-1}},
  {TS_RESETLOG, 0, 0, {-1}},
  {TS_BACKUPDBINFO, 0, 0, {-1}},
  {TS_UNLOADDBINFO, 0, 0, {-1}},
  {TS_GETTRANINFO, 0, 0, {-1}},
  {TS_KILLTRAN, 0, 0, {-1}},
  {TS_LOCKDB, 0, 0, {-1}},
  {TS_GETBACKUPLIST, 0, 0, {-1}},
  {TS_BACKUPVOLINFO, 0, 0, {-1}},
  {TS_GETDBSIZE, 0, 0, {-1}},
  {TS_LOADACCESSLOG, 0, 0, {-1}},
  {TS_DELACCESSLOG, 0, 0, {-1}},
  {TS_DELERRORLOG, 0, 0, {-1}},
  {TS_CHECKDIR, 0, 0, {-1}},
  {TS_AUTOBACKUPDBERRLOG, 0, 0, {-1}},
  {TS_KILL_PROCESS, 0, 0, {-1}},
  {TS_GETENV, 0, 0, {-1}},
  {TS_GETACCESSRIGHT, 0, 0, {-1}},
  {TS_GETADDVOLSTATUS, 0, 0, {-1}},
  {TS_GETHISTORY, 0, 0, {-1}},
  {TS_SETHISTORY, 0, 0, {-1}},
  {TS_GETHISTORYFILELIST, 0, 0, {-1}},
  {TS_READHISTORYFILE, 0, 0, {-1}},
  {TS_CHECKAUTHORITY, 0, 0, {-1}},
  {TS_GETAUTOADDVOLLOG, 0, 0, {-1}},
  {TS2_GETINITUNICASINFO, 0, 0, {-1}},
  {TS2_GETUNICASINFO, 0, 0, {-1}},
  {TS2_STARTUNICAS, 0, 0, {-1}},
  {TS2_STOPUNICAS, 0, 0, {-1}},
  {TS2_GETADMINLOGINFO, 0, 0, {-1}},
  {TS2_GETLOGFILEINFO, 0, 0, {-1}},
  {TS2_ADDBROKER, 0, 0, {-1}},
  {TS2_GETADDBROKERINFO, 0, 0, {-1}},
  {TS2_DELETEBROKER, 0, 0, {-1}},
  {TS2_RENAMEBROKER, 0, 0, {-1}},
  {TS2_GETBROKERSTATUS, 0, 0, {-1}},
  {TS2_GETBROKERCONF, 0, 0, {-1}},
  {TS2_GETBROKERONCONF, 0, 0, {-1}},
  {TS2_SETBROKERCONF, 0, 0, {-1}},
  {TS2_SETBROKERONCONF, 0, 0, {-1}},
  {TS2_STARTBROKER, 0, 0, {-1}},
  {TS2_STOPBROKER, 0, 0, {-1}},
  {TS2_SUSPENDBROKER, 0, 0, {-1}},
  {TS2_RESUMEBROKER, 0, 0, {-1}},
  {TS2_BROKERJOBFIRST, 0, 0, {-1}},
  {TS2_BROKERJOBINFO, 0, 0, {-1}},
  {TS2_ADDBROKERAS, 0, 0, {-1}},
  {TS2_DROPBROKERAS, 0, 0, {-1}},
  {TS2_RESTARTBROKERAS, 0, 0, {-1}},
  {TS2_GETBROKERSTATUSLOG, 0, 0, {-1}},
  {TS2_GETBROKERMCONF, 0, 0, {-1}},
  {TS2_SETBROKERMCONF, 0, 0, {-1}},
  {TS2_GETBROKERASLIMIT, 0, 0, {-1}},
  {TS2_GETBROKERENVINFO, 0, 0, {-1}},
  {TS2_SETBROKERENVINFO, 0, 0, {-1}},
  {TS2_ACCESSLISTADDIP, 0, 0, {-1}},
  {TS2_ACCESSLISTDELETEIP, 0, 0, {-1}},
  {TS2_ACCESSLISTINFO, 0, 0, {-1}},
#endif
  {TS_DBMTUSERLOGIN, 1, 1, {-1}},
  {TS_CHANGEOWNER, 1, 1, {TS_DELETEDB, TS_RENAMEDB, TS_STOPDB, -1}},
  {TS_REMOVE_LOG, 0, 0, {-1}},
  {TS_UNDEFINED, 0, 0, {-1}}
};

static const int monitor_task_table[] = {
  TS_GETHOSTSTAT,
  TS_GETDBPROCSTAT,
  TS_STATDUMP,
  TS2_GETBROKERSTATUS,
  TS_GET_BROKER_DIAGDATA,
  TS_GET_STANDBY_SERVER_STAT,
  TS_HEARTBEAT_LIST,
  TS_GETDBMODE,
  TS_GETSTATUSTEMPLATE,
  TS_UNDEFINED,
};

static int
is_monitor_task (char *task_entered)
{
  int retval = 0;
  int tr_num = TS_UNDEFINED;
  int i;

  tr_num = ut_get_task_info (task_entered, NULL, NULL, NULL);

  for (i = 0; monitor_task_table[i] != TS_UNDEFINED; i++)
    {
      if (tr_num == monitor_task_table[i])
	{
	  retval = 1;
	  break;
	}
    }

  return retval;
}

static int
is_conflict (char *task_entered, char *dbname_entered)
{
#if defined(WINDOWS)
  HANDLE handle;
  WIN32_FIND_DATA data;
  int found;
  char find_file[512];
#else
  DIR *dp;
  struct dirent *dirp;
#endif
  int te_num, tr_num, i, j;	/* each task number */
  int retval = -1;

#if defined(WINDOWS)
  sprintf (find_file, "%s/DBMT_comm_*.req", sco.dbmt_tmp_dir);
  handle = FindFirstFile (find_file, &data);
  if (handle == INVALID_HANDLE_VALUE)
    return 0;
#else
  dp = opendir (sco.dbmt_tmp_dir);
  if (dp == NULL)
    return 0;
#endif

#if defined(WINDOWS)
  for (found = 1; found; found = FindNextFile (handle, &data))
#else
  while ((dirp = readdir (dp)) != NULL)
#endif
    {
      /* if comm file is found, read task from it */
#if !defined(WINDOWS)
      int len = strlen (dirp->d_name);

      /*
       * if the len is small than 14 (length of ".req" + length of "DBMT_comm_")
       * continue.
       */
      if (len < 14)
	continue;

      if ((strcmp (dirp->d_name + len - 4, ".req") == 0) &&
	  (strncmp (dirp->d_name, "DBMT_comm_", 10) == 0))
#endif
	{
	  FILE *infile;
	  char strbuf[512];
	  char task_running[512], dbname_running[512];

#if defined(WINDOWS)
	  snprintf (strbuf, 512, "%s/%s", sco.dbmt_tmp_dir, data.cFileName);
#else
	  snprintf (strbuf, 512, "%s/%s", sco.dbmt_tmp_dir, dirp->d_name);
#endif
	  infile = fopen (strbuf, "r");
	  if (infile == NULL)
	    continue;
	  memset (task_running, 0, sizeof (task_running));
	  memset (dbname_running, 0, sizeof (dbname_running));
	  while (fgets (strbuf, sizeof (strbuf), infile))
	    {
	      if (!strncmp (strbuf, "task:", 5))
		strcpy (task_running, strbuf + 5);
	      if (!strncmp (strbuf, "dbname:", 7))
		strcpy (dbname_running, strbuf + 7);
	    }
	  fclose (infile);
	  uRemoveCRLF (task_running);
	  uRemoveCRLF (dbname_running);

	  /* now check if new task conflicts with it */
	  tr_num = ut_get_task_info (task_running, NULL, NULL, NULL);
	  te_num = ut_get_task_info (task_entered, NULL, NULL, NULL);
	  /* for each entry in cft_set */
	  retval = -1;
	  for (i = 0; cft_table[i].run_task != TS_UNDEFINED; ++i)
	    {
	      /* locate the entry with task num of currently running task */
	      if (cft_table[i].run_task == tr_num)
		{
		  if (cft_table[i].check_task == 1)
		    {
		      retval = 0;
		      for (j = 0; cft_table[i].conflict_task[j] > 0; ++j)
			{
			  if (cft_table[i].conflict_task[j] == te_num)
			    retval = 1;
			}
		    }

		  if ((cft_table[i].check_dbname == 1) && (retval != 0))
		    {
		      if (uStringEqual (dbname_entered, dbname_running))
			retval = 1;
		      else
			retval = 0;
		    }

		  if (retval == 1)
		    {		/* conflict detected */
#if defined(WINDOWS)
		      FindClose (handle);
#else
		      closedir (dp);
#endif
		      return 1;
		    }
		}
	    }
	}
    }
#if defined(WINDOWS)
  FindClose (handle);
#else
  closedir (dp);
#endif

  return 0;			/* no conflict found */
}

static int
is_process_running (const char *process_name, unsigned int sleep_time)
{
  FILE *input;
  char buf[16], cmd[PATH_MAX];

  SLEEP_SEC (sleep_time);

#if !defined (DO_NOT_USE_CUBRIDENV)
  sprintf (cmd, "%s/%s/%s getpid", sco.szCubrid, CUBRID_DIR_BIN,
	   process_name);
#else
  sprintf (cmd, "%s/%s getpid", CUBRID_BINDIR, process_name);
#endif
  input = popen (cmd, "r");
  if (input == NULL)
    return 0;

  memset (buf, '\0', sizeof (buf));
  if ((fgets (buf, 16, input) == NULL) || atoi (buf) <= 0)
    {
      pclose (input);
      return 0;
    }

  pclose (input);

  return 1;
}

#define FILE_BUFFER     (1024)

static int
update_port_in_httpd_conf (int supportWebManager)
{
  char conf_path[CUBRID_CMD_NAME_LEN];
  char tmp_path[CUBRID_CMD_NAME_LEN];
  char buffer[FILE_BUFFER];
  long file_size = 0;
  char *pflag = NULL;
  FILE *fp = NULL, *ftmp = NULL;
  snprintf (conf_path, CUBRID_CMD_NAME_LEN, "%s/%s/%s",
	    sco.szCubrid, DBMT_CONF_DIR, "cm_ext.conf");
  snprintf (tmp_path, CUBRID_CMD_NAME_LEN, "%s/%s/%s",
	    sco.szCubrid, DBMT_CONF_DIR, "cm_ext.conf.tmp");
  fp = fopen (conf_path, "r");
  if (fp == NULL)
    {
      return 1;
    }
  ftmp = fopen (tmp_path, "a+");
  if (ftmp == NULL)
    {
      fclose (fp);
      return 1;
    }

  while (fgets (buffer, FILE_BUFFER, fp))
    {
      fputs (buffer, ftmp);
      pflag = strstr (buffer, "-*@*-");
      if (pflag == NULL)
	{
	  continue;
	}

      if (fgets (buffer, FILE_BUFFER, fp))
	{
	  if (strstr (buffer, "server"))
	    {
	      fprintf (ftmp, "        server   localhost:%d;\n",
		       sco.iInternal_port);
	    }
	  else if (strstr (buffer, "proxy_pass"))
	    {
	      fprintf (ftmp,
		       "            proxy_pass   http://localhost:%d;\n",
		       sco.iInternal_port);
	    }
	  else if (strstr (buffer, "listen"))
	    {
	      fprintf (ftmp, "        listen       %d;\n", sco.iCMS_port);
	    }
	  else if (strstr (buffer, "location"))
	    {
	      int i = 0;
	      for (i = 0; i < 4; i++)
		{
		  if (!fgets (buffer, FILE_BUFFER, fp))
		    {
		      return 1;
		    }
		  if (strstr (buffer, "}"))
		    {
		      break;
		    }
		}
	      if (supportWebManager)
		{

		  fprintf (ftmp, "        location / {\n");
		  fprintf (ftmp, "            root   share/webmanager;\n");
		  fprintf (ftmp,
			   "            index  index.html index.htm;\n");
		  fprintf (ftmp,
			   "            add_header Cache-Control no-cache;\n");
		  fprintf (ftmp, "        }\n");
		}
	      else
		{
		  fprintf (ftmp, "        #location / {\n");
		  fprintf (ftmp, "        #    root   share/webmanager;\n");
		  fprintf (ftmp,
			   "        #    index  index.html index.htm;\n");
		  fprintf (ftmp,
			   "        #    add_header Cache-Control no-cache;\n");
		  fprintf (ftmp, "        #}\n");
		}
	    }
	}
    }

  fclose (fp);
  fclose (ftmp);

  unlink (conf_path);
  rename (tmp_path, conf_path);

  return 0;
}

static int
is_httpd_running ()
{
  char pid_path[CUBRID_CMD_NAME_LEN];
  FILE *pidfile = NULL;
  char buffer[FILE_BUFFER];
  int pidnum;
  snprintf (pid_path, CUBRID_CMD_NAME_LEN,
	    "%s/var/manager/cub_cmserver_ext.pid", sco.szCubrid);
  pidfile = fopen (pid_path, "rt");
  if (pidfile != NULL)
    {
      fscanf (pidfile, "%d", &pidnum);
      fclose (pidfile);
    }
  if (pidfile == NULL || ((kill (pidnum, 0) < 0) && (errno == ESRCH)))
    {
      return 0;
    }

  return pidnum;
}


static int
cms_start (int start_flag)
{
  char cmd_name[CUBRID_CMD_NAME_LEN];
  const char *argv[8];
  int argc = 0, exit_code = 0;
  int is_running = 0;

  /* in start, if cmserver already run, print running */
  if (start_flag && is_process_running (UTIL_CM_SERVER, 1))
    {
      fprintf (start_log_fp, "Error : Manager server already running!\n");
      return 0;
    }

  snprintf (cmd_name, CUBRID_CMD_NAME_LEN, "%s/%s/%s", sco.szCubrid,
	    CUBRID_DIR_BIN, UTIL_CM_SERVER);
  argv[argc++] = UTIL_CM_SERVER;

  if (start_flag == 0)
    {
      argv[argc++] = "stop";
    }

  argv[argc++] = NULL;

  if (ut_run_child (cmd_name, argv, 0, NULL, NULL, NULL, &exit_code) < 0)
    {
      return 1;
    }

  if (start_flag && !is_process_running (UTIL_CM_SERVER, 1))
    {
      fprintf (start_log_fp, "Error : start manager server error!\n");
      return 1;
    }
  return 0;
}

static int
httpd_start (int start_flag)
{
  char cmd_name[CUBRID_CMD_NAME_LEN];
  const char *argv[8];
  int argc = 0;
  int exit_code = 0;
  int pidnum;

  if (start_flag && update_port_in_httpd_conf (sco.iSupportWebManager))
    {
      fprintf (start_log_fp, "Error :  failed to update cm_ext.conf.\n");
      return 0;
    }

  if (cms_start (start_flag) != 0)
    {
      fprintf (start_log_fp, "Error :  cmserver start failed.\n");
      exit (1);
    }

  pidnum = is_httpd_running ();
  /* in start cmd, if httpd process cub_cmserver_ext is running, print already started */
  if (pidnum && start_flag)
    {
      fprintf (start_log_fp,
	       "Error : Manager server cub_cmserver_ext (pid=%d) already running.\n",
	       pidnum);
      return 0;
    }
  /* in stop cmd,  if httpd process cub_cmserver_ext is not running, print not running */
  if (pidnum == 0 && start_flag == 0)
    {
      fprintf (start_log_fp,
	       "Error : Manager server cub_cmserver_ext is not running.\n");
      return 0;
    }

  sprintf (cmd_name, "%s/%s/%s", sco.szCubrid, CUBRID_DIR_BIN,
	   UTIL_WEB_SERVER);
  argv[argc++] = UTIL_WEB_SERVER;

  if (start_flag == 0)
    {
      argv[argc++] = "stop";
    }

  argv[argc++] = NULL;

  if (ut_run_child (cmd_name, argv, 1, NULL, NULL, NULL, &exit_code) < 0
      || exit_code != 0)
    {
      if (start_flag != 0)
	{
	  fprintf (start_log_fp,
		   "Error : Can not start process cub_cmserver_ext.\n");
	  cms_start (0);
	  exit (1);
	}
      else
	{
	  fprintf (start_log_fp,
		   "Error : Can not stop process cub_cmserver_ext.\n");
	}
    }

  return 0;
}

static void
service_start (void)
{
  while (1)
    {
      SLEEP_SEC (1);
    }
}

#if 0
static void
service_start (void)
{
  SOCKET pserver_con_fd;
  SOCKET fserver_con_fd;
  SOCKET maxfd;

  fd_set read_set;

  struct sockaddr_in cli_addr;
  T_SOCKLEN clilen;
  T_THREAD fserver_slave_id;
  T_THR_ARG *thr_arg;
  volatile unsigned int req_count = 0;

  T_KEEP_ALIVE_INFO keep_alive_info[MAX_CLIENT_NUM];

  int i;			// loop var 
  int max_client = 0;

  char tmp[1024];

  sprintf (tmp, "fserver_fd = %d, pserver_fd = %d", fserver_sockfd,
	   pserver_sockfd);
  ut_access_log (NULL, tmp);

  for (i = 0; i < MAX_CLIENT_NUM; ++i)
    {
      reset_keep_alive_info (&keep_alive_info[i]);
    }

  for (;;)
    {
      clilen = sizeof (cli_addr);
      FD_ZERO (&read_set);
      FD_SET (pserver_sockfd, &read_set);
      FD_SET (fserver_sockfd, &read_set);

      maxfd =
	pserver_sockfd > fserver_sockfd ? pserver_sockfd : fserver_sockfd;
      for (i = 0; i < MAX_CLIENT_NUM; ++i)
	{
	  int sock_fd = keep_alive_info[i].sock_fd;
	  if (!IS_INVALID_SOCKET (sock_fd))
	    {
	      if (sock_fd > maxfd)
		{
		  maxfd = sock_fd;
		}
	      FD_SET (sock_fd, &read_set);
	    }
	}

      if (select (maxfd + 1, &read_set, NULL, NULL, NULL) < 0)
	{
	  continue;
	}

      //ut_access_log(NULL, "after select func."); 

      if (FD_ISSET (pserver_sockfd, &read_set))
	{

	  ut_access_log (NULL, "pserver begin.");

	  pserver_con_fd =
	    accept (pserver_sockfd, (struct sockaddr *) &cli_addr, &clilen);

	  if (pserver_con_fd > 0)
	    {
	      ut_access_log (NULL, "pserver_con_fd is valid.");

	      req_count++;

	      thr_arg = (T_THR_ARG *) MALLOC (sizeof (T_THR_ARG));
	      if (thr_arg == NULL)
		{
		  CLOSE_SOCKET (pserver_con_fd);
		  break;
		}
	      thr_arg->clt_sock_fd = pserver_con_fd;
	      thr_arg->req_count = req_count;
	      thr_arg->keep_alive_info = keep_alive_info;

	      MUTEX_LOCK (mutex_thread_num);
	      if (job_num == sco.iConcurrent_Thread_NUM)
		{
		  COND_WAIT (cond_thread_num, mutex_thread_num);
		}

	      ut_access_log (NULL, "before fserver_slave_thr_f.");
	      THREAD_BEGIN (fserver_slave_id, fserver_slave_thr_f, thr_arg);

	      if (fserver_slave_id != 0)
		{
		  job_num++;
		}
	      MUTEX_UNLOCK (mutex_thread_num);

	      ut_access_log (NULL, "pserver running..");
	    }
	}

      if (FD_ISSET (fserver_sockfd, &read_set))
	{
	  ut_access_log (NULL, "fserver begin.");

	  fserver_con_fd =
	    accept (fserver_sockfd, (struct sockaddr *) &cli_addr, &clilen);

	  if (fserver_con_fd > 0)
	    {

	      ut_access_log (NULL, "fserver_con_fd is valid.");
	      req_count++;

	      thr_arg = (T_THR_ARG *) MALLOC (sizeof (T_THR_ARG));
	      if (thr_arg == NULL)
		{
		  CLOSE_SOCKET (fserver_con_fd);
		  break;
		}
	      thr_arg->clt_sock_fd = fserver_con_fd;
	      thr_arg->req_count = req_count;

	      MUTEX_LOCK (mutex_thread_num);
	      if (job_num == sco.iConcurrent_Thread_NUM)
		{
		  COND_WAIT (cond_thread_num, mutex_thread_num);
		}

	      THREAD_BEGIN (fserver_slave_id, fserver_slave_thr_f, thr_arg);

	      if (fserver_slave_id != 0)
		{
		  job_num++;
		}
	      MUTEX_UNLOCK (mutex_thread_num);
	    }
	}

      for (i = 0; i < MAX_CLIENT_NUM; ++i)
	{
	  int sock_fd = keep_alive_info[i].sock_fd;

	  if (!IS_INVALID_SOCKET (sock_fd) && FD_ISSET (sock_fd, &read_set))
	    {
	      time_t time_now = time (NULL);
	      nvplist *req = nv_create (5, NULL, "\n", ":", "\n");

	      if (ut_receive_request (sock_fd, req) != 0)
		{
		  CLOSE_SOCKET (sock_fd);
		  reset_keep_alive_info (&keep_alive_info[i]);
		  nv_destroy (req);
		  continue;
		}

	      /*if(time_now - keep_alive_info[i].last_time > 60) 
	         {
	         CLOSE_SOCKET(sock_fd); 
	         reset_keep_alive_info(&keep_alive_info[i]); 
	         continue; 
	         } */
	      keep_alive (sock_fd);
	      keep_alive_info[i].last_time = time_now;
	      nv_destroy (req);
	    }
	}
    }
}
#endif

static void
reset_keep_alive_info (T_KEEP_ALIVE_INFO * keep_alive_info)
{

  strcpy (keep_alive_info->user_id, "");
  keep_alive_info->sock_fd = -1;
  keep_alive_info->last_time = 0u;
}

static void
keep_alive (SOCKET sock_fd)
{
  nvplist *res;

  res = nv_create (5, NULL, "\n", ":", "\n");

  if (res != NULL)
    {
      ut_send_response (sock_fd, res);
      nv_destroy (res);
    }
}

static void
print_usage (char *pname)
{
  fprintf (start_log_fp, "Usage: %s [command]\n", pname);
  fprintf (start_log_fp, "commands are :\n");
  fprintf (start_log_fp, "   start   -  start the server\n");
  fprintf (start_log_fp, "   stop    -  stop the server\n");
}

static void
start_fserver (void)
{
#if defined(WINDOWS)
  HANDLE handle;
  WIN32_FIND_DATA data;
  int found;
  char isolation_lv_env_name[32];
  char isolation_lv_env_value[4];
#else
  DIR *dirp = NULL;
  struct dirent *dp = NULL;
  char isolation_lv_env_string[64];
#endif
  char buf[512];

  //fd_set fds; 

  /* change isolation level environment variable */
#ifdef	WINDOWS
  sprintf (isolation_lv_env_name, "CUBRID_ISOLATION_LEVEL");
  sprintf (isolation_lv_env_value, "%d",
	   CMS_TRAN_COMMIT_CLASS_UNCOMMIT_INSTANCE);

  if (SetEnvironmentVariable (isolation_lv_env_name, isolation_lv_env_value)
      == 0)
    {
      fprintf (start_log_fp,
	       "Error while setting environment variable CUBRID_ISOLATION_LEVEL ---> continue\n");
    }
#else
  sprintf (isolation_lv_env_string, "CUBRID_ISOLATION_LEVEL=%d",
	   CMS_TRAN_COMMIT_CLASS_UNCOMMIT_INSTANCE);
  if (putenv (isolation_lv_env_string) != 0)
    {
      fprintf (start_log_fp,
	       "Error while setting environment variable CUBRID_ISOLATION_LEVEL ---> continue\n");
    }
#endif

#if 0
  fprintf (start_log_fp, "Server port : %d\n", sco.iFsvr_port);
  fprintf (start_log_fp, "Access log : %s\n",
	   conf_get_dbmt_file (FID_FSERVER_ACCESS_LOG, buf));
  fprintf (start_log_fp, "Error  log : %s\n",
	   conf_get_dbmt_file (FID_FSERVER_ERROR_LOG, buf));

  fprintf (start_log_fp, "Starting server as a daemon.\n");
#endif
  /* fprintf (start_log_fp, "\ncms start ... OK\n"); */
  ut_daemon_start ();

#if defined(WINDOWS)
  SetConsoleCtrlHandler ((PHANDLER_ROUTINE) CtrlHandler, TRUE);
#else
  signal (SIGINT, term_handler);
  signal (SIGTERM, term_handler);
#endif

  //if (fserver_net_init () < 0 || pserver_net_init() < 0)
  //  exit (1);
  if (ut_write_pid (conf_get_dbmt_file (FID_FSERVER_PID, buf)) < 0)
    exit (1);

  /* initialize the mutex_thread_num obj. */
  if (MUTEX_INIT (mutex_thread_num) != 0)
    {
      fprintf (start_log_fp, "ERROR: Can't initialize mutex_thread_num. ");
      exit (1);
    }

  /* initialize the cond_thread_num obj. */
  if (COND_INIT (cond_thread_num) != 0)
    {
      fprintf (start_log_fp, "ERROR: Can't initialize cond_thread_num. ");
      exit (1);
    }

  fflush (start_log_fp);

  //FD_ZERO(&fds); 
  //FD_SET(fserver_sockfd, &fds); 
  //FD_SET(pserver_sockfd, &fds); 
  //server_fd_clear (fds);

  /* Delete any temporary from from sco.dbmt_tmp_dir */
#if defined(WINDOWS)
  sprintf (buf, "%s/*", sco.dbmt_tmp_dir);
  if ((handle = FindFirstFile (buf, &data)) != INVALID_HANDLE_VALUE)
#else
  if ((dirp = opendir (sco.dbmt_tmp_dir)) != NULL)
#endif
    {
#if defined(WINDOWS)
      for (found = 1; found; found = FindNextFile (handle, &data))
#else
      while ((dp = readdir (dirp)) != NULL)
#endif
	{
	  char *p;

#if defined(WINDOWS)
	  p = data.cFileName;
#else
	  p = dp->d_name;
#endif
	  if ((!strncmp (p, "DBMT_proc_", 10)) ||
	      (!strncmp (p, "DBMT_task_", 10)) ||
	      (!strncmp (p, "DBMT_comm_", 10)) ||
	      (!strncmp (p, "DBMT_util_", 10)) ||
	      (!strncmp (p, "DBMT_stat_", 10)) ||
	      (!strncmp (p, "DBMT_auto_", 10)))
	    {
	      sprintf (buf, "%s/%s", sco.dbmt_tmp_dir, p);
	      unlink (buf);
	    }
	}
#if defined(WINDOWS)
      FindClose (handle);
#else
      closedir (dirp);
#endif
    }

  if (1 == sco.iMon_cub_auto)
    {
      T_THREAD tid;
      void *null_ptr = NULL;
      THREAD_BEGIN (tid, mon_cub_auto, null_ptr);
    }
  service_start ();
}

#if !defined(WINDOWS)
static int
run_child_linux (const char *pname, const char *const argv[], int wait_flag,
		 const char *stdin_file, char *stdout_file, char *stderr_file,
		 int *exit_status)
{
  int pid;

  if (exit_status != NULL)
    *exit_status = 0;

  if (wait_flag)
    signal (SIGCHLD, SIG_DFL);
  else
    signal (SIGCHLD, SIG_IGN);
  pid = fork ();
  if (pid == 0)
    {
      FILE *fp;

      close_all_fds (3);

      if (stdin_file != NULL)
	{
	  fp = fopen (stdin_file, "r");
	  if (fp != NULL)
	    {
	      dup2 (fileno (fp), 0);
	      fclose (fp);
	    }
	}
      if (stdout_file != NULL)
	{
	  unlink (stdout_file);
	  fp = fopen (stdout_file, "w");
	  if (fp != NULL)
	    {
	      dup2 (fileno (fp), 1);
	      fclose (fp);
	    }
	}
      if (stderr_file != NULL)
	{
	  unlink (stderr_file);
	  fp = fopen (stderr_file, "w");
	  if (fp != NULL)
	    {
	      dup2 (fileno (fp), 2);
	      fclose (fp);
	    }
	}

      execv (pname, (char *const *) argv);
      exit (0);
    }

  if (pid < 0)
    return -1;

  if (wait_flag)
    {
      int status = 0;
      waitpid (pid, &status, 0);
      if (exit_status != NULL)
	*exit_status = status;
      return 0;
    }
  else
    {
      return pid;
    }
}
#endif


static THREAD_FUNC
mon_cub_auto (void *arg)
{
  char pid_file_name[512];
  int restart = 0;
  int pidnum = 0;
  FILE *pidfile;

  conf_get_dbmt_file (FID_PSERVER_PID, pid_file_name);

  while (1)
    {
      restart = 0;
      SLEEP_SEC (sco.iMonitorInterval);
      if (access (pid_file_name, F_OK) < 0)
	{
	  restart = 1;
	}
      else
	{
	  pidfile = fopen (pid_file_name, "rt");
	  if (pidfile != NULL)
	    {
	      fscanf (pidfile, "%d", &pidnum);
	      fclose (pidfile);
	    }

	  if (pidfile == NULL || ((kill (pidnum, 0) < 0) && (errno == ESRCH))
	      || (is_cmserver_process (pidnum, PSERVER_MODULE_NAME) == 0))
	    {
	      unlink (pid_file_name);
	      restart = 1;
	    }
	}

      if (1 == restart)
	{
	  const char *argv[5];
	  char cmd_name[CUBRID_CMD_NAME_LEN];
	  char cmd_fullname[CUBRID_CMD_NAME_LEN];

	  cmd_name[0] = '\0';
	  cmd_fullname[0] = '\0';
	  snprintf (cmd_fullname, sizeof (cmd_fullname) - 1, "%s/%s/%s",
		    sco.szCubrid, CUBRID_DIR_BIN, UTIL_CUB_AUTO_NAME);

#if defined(WINDOWS)
	  argv[0] = cmd_fullname;
	  argv[1] = "start";
	  argv[2] = NULL;
	  run_child (argv, 0, NULL, NULL, NULL, NULL);
#else
	  snprintf (cmd_name, sizeof (cmd_name) - 1, "%s",
		    UTIL_CUB_AUTO_NAME);
	  argv[0] = cmd_name;
	  argv[1] = "start";
	  argv[2] = NULL;
	  run_child_linux (cmd_fullname, argv, 0, NULL, NULL, NULL, NULL);
#endif
	  ut_access_log2 ("fsvr", "Restart cub_auto process");
	}
    }

}

int
main (int argc, char **argv)
{
  FILE *pidfile;
  int pidnum;
  char *ars_cmd;
  char conf_file[256];

#if !defined(WINDOWS)
  signal (SIGCHLD, SIG_IGN);
  signal (SIGPIPE, SIG_IGN);
#endif

#if defined(WINDOWS)
  FreeConsole ();
  start_log_fp = fopen ("cub_jsstart.log", "w");
  if (start_log_fp == NULL)
    start_log_fp = stdout;
#else
  start_log_fp = stdout;
#endif

  if (argc != 2)
    {
      fprintf (start_log_fp,
	       "Error : Wrong number of command line arguments.\n");
      print_usage (argv[0]);
      exit (1);
    }
  ars_cmd = argv[1];

  if (strcmp (ars_cmd, "--version") == 0)
    {
      fprintf (start_log_fp, "CUBRID Manager Server ver : %s\n",
	       makestring (BUILD_NUMBER));
      exit (1);
    }

#if 0
  fprintf (start_log_fp, "cub_js %s\n", makestring (BUILD_NUMBER));
#endif

  sys_config_init ();

  if (uReadEnvVariables (argv[0], start_log_fp) < 0)
    {
      fprintf (start_log_fp, "Error while reading environment variables.\n");
      exit (1);
    }

  if (uReadSystemConfig () < 0)
    {
      fprintf (start_log_fp,
	       "Can't access system configuration file (%s): No such file or directory\n",
	       conf_get_dbmt_file (FID_DBMT_CONF, conf_file));
      exit (1);
    }

  make_default_env ();

  /* check system configuration */
  if (uCheckSystemConfig (start_log_fp) < 0)
    {
      fprintf (start_log_fp,
	       "Error while checking system configuration file.\n");
      exit (1);
    }

  if (strcmp (ars_cmd, "start") == 0)
    {
      char tmpfile[512];

      SLEEP_MILISEC (0, 100);
      uRemoveLockFile (uCreateLockFile
		       (conf_get_dbmt_file (FID_PSERVER_PID_LOCK, tmpfile)));

      conf_get_dbmt_file (FID_PSERVER_PID, tmpfile);
      if (access (tmpfile, F_OK) < 0)
	{
	  fprintf (start_log_fp, "Error : %s not found.\n", tmpfile);
	  fprintf (start_log_fp,
		   "      : cub_auto may not be running. Start cub_auto first.\n");
	  exit (1);
	}

      conf_get_dbmt_file (FID_FSERVER_PID, tmpfile);
      strcpy (g_pidfile_path, tmpfile);

      if (access (tmpfile, F_OK) < 0)
	{
	  httpd_start (1);
	  start_fserver ();
	}
      else
	{
	  httpd_start (1);

	  pidfile = fopen (tmpfile, "rt");
	  if (pidfile == NULL)
	    {
	      fprintf (start_log_fp,
		       "Error : pid file exists, but can not open it[%s]",
		       tmpfile);
	    }
	  else
	    {
	      fscanf (pidfile, "%d", &pidnum);
	      fclose (pidfile);

	      if (((kill (pidnum, 0) < 0) && (errno == ESRCH))
		  || (is_cmserver_process (pidnum, FSERVER_MODULE_NAME) == 0))
		{
		  /* fprintf (start_log_fp, "Previous pid file found. Removing and proceeding ...\n"); */
		  unlink (tmpfile);
		  start_fserver ();
		}
	      else
		{
		  fprintf (start_log_fp,
			   "Error : Job Server[pid=%d] already running.\n",
			   pidnum);
		}
	    }
	}
    }
  else if (strcmp (ars_cmd, "stop") == 0)
    {
      char tmpfile[512];
      httpd_start (0);
      conf_get_dbmt_file (FID_FSERVER_PID, tmpfile);
      if (access (tmpfile, F_OK) < 0)
	{
	  fprintf (start_log_fp,
		   "Error : Can not stop. Server not running.\n");
	  exit (1);
	}
      else
	{
	  pidfile = fopen (tmpfile, "rt");
	  if (pidfile != NULL)
	    {
	      fscanf (pidfile, "%d", &pidnum);
	      fclose (pidfile);
	    }

	  /* fprintf (start_log_fp, "Stopping server process with pid %d\n", pidnum); */
	  if ((pidfile == NULL) || ((kill (pidnum, SIGTERM)) < 0))
	    {
	      fprintf (start_log_fp, "Error : Failed to stop the server.\n");
	    }
	  else
	    {
	      unlink (tmpfile);
	    }
	}
    }
  else if (strcmp (ars_cmd, "getpid") == 0)
    {
      char tmpfile[512];

      conf_get_dbmt_file (FID_FSERVER_PID, tmpfile);
      strcpy (g_pidfile_path, tmpfile);

      if (access (tmpfile, F_OK) < 0)
	{
	  exit (1);
	}
      else
	{
	  pidfile = fopen (tmpfile, "rt");
	  if (pidfile != NULL)
	    {
	      fscanf (pidfile, "%d", &pidnum);
	      fclose (pidfile);
	    }
	  if (pidfile == NULL || ((kill (pidnum, 0) < 0) && (errno == ESRCH))
	      || (is_cmserver_process (pidnum, FSERVER_MODULE_NAME) == 0))
	    {
	      exit (1);
	    }
	  else
	    {
	      fprintf (stdout, "%d\n", pidnum);
	      exit (0);
	    }
	}
    }
  else
    {
      fprintf (start_log_fp, "Error : Invalid command - %s\n", ars_cmd);
      print_usage (argv[0]);
      exit (1);
    }

  return 0;
}

#if 0
static int
pserver_net_init (void)
{
  int optval = 1;
  struct sockaddr_in serv_addr;

#if defined(WINDOWS)
  if (wsa_initialize () < 0)
    {
      return -1;
    }
#endif

  /* set up network */
  pserver_sockfd = socket (AF_INET, SOCK_STREAM, 0);
  if (IS_INVALID_SOCKET (pserver_sockfd))
    {
      perror ("socket");
      return -1;
    }
  memset ((char *) &serv_addr, 0, sizeof (serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl (INADDR_ANY);
  serv_addr.sin_port = htons ((unsigned short) sco.iPsvr_port);

  if (setsockopt
      (pserver_sockfd, SOL_SOCKET, SO_REUSEADDR, (const char *) &optval,
       sizeof (optval)) < 0)
    {
      perror ("setsockopt");
      return -1;
    }
  if (bind
      (pserver_sockfd, (struct sockaddr *) &serv_addr,
       sizeof (serv_addr)) < 0)
    {
      perror ("bind");
      return -1;
    }

  if (listen (pserver_sockfd, 5) < 0)
    {
      perror ("listen");
      return -1;
    }
  return 0;
}
#endif

#if 0
static int
fserver_net_init (void)
{
  int optval = 1;
  struct sockaddr_in serv_addr;

#if defined(WINDOWS)
  if (wsa_initialize () < 0)
    return -1;
#endif

  fserver_sockfd = socket (AF_INET, SOCK_STREAM, 0);
  if (IS_INVALID_SOCKET (fserver_sockfd))
    {
      perror ("socket");
      return -1;
    }

  memset ((char *) &serv_addr, 0, sizeof (serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl (INADDR_ANY);
  serv_addr.sin_port = htons ((unsigned short) sco.iFsvr_port);

  if (setsockopt
      (fserver_sockfd, SOL_SOCKET, SO_REUSEADDR, (const char *) &optval,
       sizeof (optval)) < 0)
    {
      perror ("setsockopt");
      return -1;
    }

  if ((bind
       (fserver_sockfd, (struct sockaddr *) &serv_addr,
	sizeof (serv_addr))) < 0)
    {
      perror ("bind");
      return -1;
    }
  if ((listen (fserver_sockfd, 100)) < 0)
    {
      perror ("listen");
      return -1;
    }
  return 0;
}
#endif

static int
send_res_file (SOCKET sock_fd, char *filename, char *errmsg, int errmsg_len)
{
  FILE *fp;
  char buf_t[1024];
  dstring *buf_line = NULL;

  buf_line = dst_create ();

  fp = fopen (filename, "r");
  if (fp == NULL)
    {
      snprintf (errmsg, errmsg_len - 1, "Open Response file(%s) error: %s",
		filename, strerror (errno));
      return -1;
    }

  while (fgets (buf_t, sizeof (buf_t), fp))
    {
      char *buf = NULL;

      dst_append (buf_line, buf_t, strlen (buf_t));
      buf = dst_buffer (buf_line);

      if (buf[strlen (buf) - 1] != '\n')
	continue;

      buf = dst_buffer (buf_line);

      if (strncmp (buf, ENCRYPT_SIGN, strlen (ENCRYPT_SIGN)) == 0)
	{
	  int len;
	  char *p, *q;
	  char *encrypt_str;

	  if ((p = strchr (buf, ':')) == NULL)
	    {
	      strcpy_limit (errmsg, "Request task format error: missing (:)",
			    errmsg_len);
	      fclose (fp);
	      return -1;
	    }
	  *p = '\0';
	  p++;
	  if ((q = strchr (p, '\n')) == NULL)
	    {
	      strcpy_limit (errmsg,
			    "Request task format error: missing (\\n)",
			    errmsg_len);
	      fclose (fp);
	      return -1;
	    }
	  *q = '\0';

	  len = strlen (p);
	  len = MAX (MIN_ENCRYPT_LEN, len);
	  /* make the len to be mutiple of 8 to encrypt. */
	  len = MAKE_MUTIPLE_EIGHT (len);

	  if ((encrypt_str = (char *) MALLOC (len * 2 + 1)) == NULL)
	    {
	      strcpy_limit (errmsg, "Memory Alloc Error.", errmsg_len);
	      fclose (fp);
	      return -1;
	    }

	  uEncrypt (len, p, encrypt_str);

	  write_to_socket (sock_fd, buf, strlen (buf));
	  write_to_socket (sock_fd, ":", 1);
	  write_to_socket (sock_fd, encrypt_str, strlen (encrypt_str));
	  write_to_socket (sock_fd, "\n", 1);

	  FREE_MEM (encrypt_str);
	}
      else
	{
	  write_to_socket (sock_fd, buf, strlen (buf));
	}

      dst_reset (buf_line);
    }

  dst_destroy (buf_line);
  fclose (fp);
  return 0;
}

static void
make_fail_response (nvplist * res, const char *task, const char *errmsg)
{
  nv_add_nvp (res, "task", task);
  nv_add_nvp (res, "status", "failure");
  nv_add_nvp (res, "note", errmsg);
}
